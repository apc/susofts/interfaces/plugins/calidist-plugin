#include <Logger.hpp>

#include "CaliDistPlugin.hpp"
#include "Version.hpp"
#include "trinity/CaliDistConfig.hpp"
#include "trinity/CaliDistGraphicalWidget.hpp"
#include "trinity/CaliDistLaunchObject.hpp"

class CaliDistPlugin::_CaliDistPlugin_pimpl
{
public:
};

CaliDistPlugin::CaliDistPlugin(QObject *parent) : QObject(parent), SPluginInterface(), _pimpl(std::make_unique<_CaliDistPlugin_pimpl>())
{
}

CaliDistPlugin::~CaliDistPlugin() = default;

QIcon CaliDistPlugin::icon() const
{
	return QIcon(":/icons/calidist");
}

void CaliDistPlugin::init()
{
	logDebug() << "Plugin CaliDist loaded";
}

void CaliDistPlugin::terminate()
{
	logDebug() << "Plugin CaliDist unloaded";
}

SConfigWidget *CaliDistPlugin::configInterface(QWidget *parent)
{
	logDebug() << "loading CaliDistPlugin::configInterface (CaliDistConfig)";
	return new CaliDistConfig(this, parent);
}

SGraphicalWidget *CaliDistPlugin::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading CaliDistlugin::graphicalInterface (CaliDistGraphicalWidget)";
	// return new CaliDistGraphicalWidget(this, parent);
	return nullptr;
}

SLaunchObject *CaliDistPlugin::launchInterface(QObject *parent)
{
	logDebug() << "loading CaliDistPlugin::launchInterface (CaliDistLaunchObject)";
	return new CaliDistLaunchObject(this, parent);
}

const QString &CaliDistPlugin::_name()
{
	static const QString _sname = QString::fromStdString(getPluginName());
	return _sname;
}

const QString &CaliDistPlugin::_baseMimetype()
{
	static const QString _sbaseMimetype = "application/vnd.cern.susoft.surveypad.plugin.calidist";
	return _sbaseMimetype;
}
