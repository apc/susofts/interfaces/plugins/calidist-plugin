#include "CaliDistLaunchObject.hpp"

#include <QProcess>

#include <utils/SettingsManager.hpp>

#include "CaliDistPlugin.hpp"
#include "trinity/CaliDistConfig.hpp"

QString CaliDistLaunchObject::exepath() const
{
	auto conf = SettingsManager::settings().settings<CaliDistConfigObject>(CaliDistPlugin::_name());
	return conf.calidistPath;
}

void CaliDistLaunchObject::launch(const QString &file)
{
	process().setArguments({file});
	process().setProgram(exepath());
	ProcessLauncherObject::launch(file);
}
