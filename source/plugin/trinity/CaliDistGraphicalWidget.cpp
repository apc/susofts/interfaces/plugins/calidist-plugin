#include "CaliDistGraphicalWidget.hpp"

#include <QFileInfo>
#include <QTabWidget>

#include "CaliDistPlugin.hpp"

class CaliDistGraphicalWidget::_CaliDistGraphicalWidget_pimpl
{
public:
};

CaliDistGraphicalWidget::CaliDistGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	STabInterface(owner, parent), _pimpl(std::make_unique<_CaliDistGraphicalWidget_pimpl>())
{
}

CaliDistGraphicalWidget::~CaliDistGraphicalWidget() = default;

QString CaliDistGraphicalWidget::getPath() const
{
	auto *currentTab = qobject_cast<SGraphicalWidget *>(tab()->currentWidget());
	if (currentTab)
		return QFileInfo(currentTab->getPath()).absoluteFilePath();
	return QString();
}
