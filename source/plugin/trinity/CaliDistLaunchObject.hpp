/*
© Copyright CERN 2000-2021. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CALIDISTLAUNCHOBJECT_HPP
#define CALIDISTLAUNCHOBJECT_HPP

#include <interface/ProcessLauncherObject.hpp>

class CaliDistLaunchObject : public ProcessLauncherObject
{
	Q_OBJECT

public:
	CaliDistLaunchObject(SPluginInterface *owner, QObject *parent = nullptr) : ProcessLauncherObject(owner, parent) {}
	virtual ~CaliDistLaunchObject() override = default;

	// SLaunchObject
	virtual QString exepath() const override;

public slots:
	// ProcessLauncherObject
	virtual void launch(const QString &file) override;
};

#endif // CALIDISTLAUNCHOBJECT_HPP
