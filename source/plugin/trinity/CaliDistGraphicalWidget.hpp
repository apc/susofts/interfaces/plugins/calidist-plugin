/*
© Copyright CERN 2000-2021. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CALIDISTGRAPHICALWIDGET_HPP
#define CALIDISTGRAPHICALWIDGET_HPP

#include <interface/STabInterface.hpp>

class CaliDistGraphicalWidget : public STabInterface
{
	Q_OBJECT

public:
	CaliDistGraphicalWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~CaliDistGraphicalWidget() override;

	// SgraphicalWidget
	virtual QString getPath() const override;

public slots:
	// SGraphicalWidget
	virtual void updateUi(const QString &) override {}
	virtual void aboutTorun() override {}
	virtual void runFinished(bool) override {}

private:
	class _CaliDistGraphicalWidget_pimpl;
	std::unique_ptr<_CaliDistGraphicalWidget_pimpl> _pimpl;
};

#endif // CALIDISTGRAPHICALWIDGET_HPP
