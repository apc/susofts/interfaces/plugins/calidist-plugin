/*
© Copyright CERN 2000-2021. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CALIDISTCONFIG_HPP
#define CALIDISTCONFIG_HPP

#include <memory>

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class CaliDistConfig;
}

struct CaliDistConfigObject : public SConfigObject
{
	virtual ~CaliDistConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/** Path to CaliDist executable */
	QString calidistPath = "C:/Program Files/SUSoft/CaliDist/2.01.00/CaliDistGUI.exe";
};

class CaliDistConfig : public SConfigWidget
{
	Q_OBJECT

public:
	CaliDistConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~CaliDistConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private slots:
	void selectPath();

private:
	std::unique_ptr<Ui::CaliDistConfig> ui;
};

#endif // CALIDISTCONFIG_HPP
