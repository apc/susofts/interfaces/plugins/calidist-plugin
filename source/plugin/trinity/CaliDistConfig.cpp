#include "CaliDistConfig.hpp"
#include "ui_CaliDistConfig.h"

#include <QFileDialog>
#include <QSettings>

#include "CaliDistPlugin.hpp"
#include "Version.hpp"

bool CaliDistConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const CaliDistConfigObject &oc = static_cast<const CaliDistConfigObject &>(o);
	return calidistPath == oc.calidistPath;
}

void CaliDistConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(CaliDistPlugin::_name());

	settings.setValue("calidistPath_v2.01.00", calidistPath);

	settings.endGroup();
}

void CaliDistConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(CaliDistPlugin::_name());

	calidistPath = settings.value("calidistPath_v2.01.00", "C:/Program Files/SUSoft/CaliDist/2.01.00/CaliDistGUI.exe").toString();

	settings.endGroup();
}

CaliDistConfig::CaliDistConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::CaliDistConfig>())
{
	ui->setupUi(this);
	ui->lblVersion->setText(ui->lblVersion->text().arg(QString::fromStdString(getVersion())));
}

CaliDistConfig::~CaliDistConfig() = default;

SConfigObject *CaliDistConfig::config() const
{
	auto *tmp = new CaliDistConfigObject();
	tmp->calidistPath = QDir::fromNativeSeparators(ui->lineExePath->text());
	return tmp;
}

void CaliDistConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const CaliDistConfigObject *>(conf);
	if (config)
	{
		ui->lineExePath->setText(QDir::toNativeSeparators(config->calidistPath));
	}
}

void CaliDistConfig::reset()
{
	CaliDistConfigObject config;
	config.read();
	setConfig(&config);
}

void CaliDistConfig::restoreDefaults()
{
	CaliDistConfigObject config;
	setConfig(&config);
}

void CaliDistConfig::selectPath()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("CaliDist executable file"), ui->lineExePath->text(), tr("CaliDistGUI.exe (CaliDistGUI.exe);;Executable (*.exe);;Any (*.*)"));
	if (!filename.isEmpty())
		ui->lineExePath->setText(QDir::toNativeSeparators(filename));
}
